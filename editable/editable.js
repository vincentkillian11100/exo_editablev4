//MODEL
addhtml()
let requestPerson = "../data/person.json";
let requestPoints = "../data/points.json";
let requestProducts = "../data/product.json";
let section = document.getElementById("area_tab")
let data;
let tab;


function dataPerson(){
  let url;
  fetch(requestPerson) // j'envoie ma requête
      .then(
      rep => rep.json()
    )
      .then(
        dataJson => {
          data = dataJson;
          creatHtml()
      }
    )
      .catch(err => console.log(err))
    section.innerHTML = ""
};

function dataPoints () {
  let url;
  fetch(requestPoints) // j'envoie ma requête
      .then(
      rep => rep.json()
    )
      .then(
        dataJson => {
          data = dataJson;
          creatHtml()
      }
    )
      .catch(err => console.log(err))
    section.innerHTML = ""
};

function dataProduct() {
  let url;
  fetch(requestProducts) // j'envoie ma requête
      .then(
      rep => rep.json()
    )
      .then(
        dataJson => {
          data = dataJson;
          creatHtml()
      }
    )
      .catch(err => console.log(err))
    section.innerHTML = ""
    };


//VIEW
function updateData(obj_index, key, value) {
  data[obj_index][key] = value;

}


 


function creatHtml() {
  let container_tab = document.getElementById("area_tab")
  tab = document.createElement('table');
  tab.setAttribute('border-spacing','0')
  tab.classList.add('tab')
  container_tab.appendChild(tab)
  headTab()
  bodyTab()
}

function headTab() {
  let header = document.createElement('thead')
  let tr = document.createElement('tr')
  tr.classList.add("tr_head")
  tr.setAttribute('colspan', '0') 
  let obj = data[0]
  for (const objKey in obj) {
    let th = document.createElement('th')
    th.setAttribute('border-spacing','0')
    th.innerText = objKey;
    th.addEventListener('click', trier)
    tr.appendChild(th)
  }
  header.appendChild(tr)
  tab.appendChild(header)
}

function bodyTab() {
  let tbody = document.createElement('tbody')
  let i = 0
  for (const obj of data) {
    let tr = document.createElement('tr')
    tr.classList.add("row")
    for (const key in obj) {
      let td = document.createElement('td')
      td.innerText = obj[key];
      td.contentEditable = "true"
      td.classList.add('cell')
      td.setAttribute('data-key', key)
      td.setAttribute('data-index', i)
      td.addEventListener('change', modifCell)
      tr.appendChild(td)
    }
    tbody.appendChild(tr)
    i++
  }
  let oldBody = tab.getElementsByTagName('tbody')[0]
  if (oldBody !== undefined) {
    oldBody.remove();
    }
  tab.appendChild(tbody)
}

//CONTROL
function filter(e) {
  let searchValue = e.target.value;
  let rows = document.querySelectorAll('.row')
  for (const row of rows) {
    if (row.innerText.includes(searchValue)) {
      row.style.display = 'table-row'
    } else {
      row.style.display = 'none'
    }
  }
}

function trier(e) {
  let key = e.target.textContent;
  data.sort((a, b) => a[key].toString().localeCompare(b[key].toString())
  )
  bodyTab()
}

function ajouter(e) {
  let newObj = {}
  for (const key in data[0]) {
    newObj[key] = ""
  }
  data.push(newObj)
  bodyTab()
}


function modifCell(e) {
  let value = e.target.innerText
  let key = e.target.getAttribute('data-key')
  let index = e.target.getAttribute('data-index')
  updateData(index, key, value)
}


function save(e) {
  // transforme des données en text
  let jsonData = JSON.stringify(data)
  fetch('up.php', {
    method: 'POST',
    headers: {
      "content-type": "application/json"
    },
    body: jsonData
  })
    .then(
    r => r.json()
  )
    .then(
    d => console.log(d)
  )
}


function addhtml(){
  // je n'est pas fait un innerHtml pour m'entrainer avec le javascript
    let main = document.querySelector('main')
  let container_button = document.createElement('div')
  container_button.classList.add('container_link')
  main.appendChild(container_button)

  let btnPers = document.createElement('input') // id input-search
  btnPers.setAttribute('type','button') 
  btnPers.setAttribute('id','person')
  btnPers.setAttribute('value','person')
  container_button.appendChild(btnPers)
  btnPers.addEventListener('click', dataPerson)
  
  let btnPoints  = document.createElement('input') // id input-search
  btnPoints.setAttribute('type','button') 
  btnPoints.setAttribute('id','points')
  btnPoints.setAttribute('value','points')  
  container_button.appendChild(btnPoints)
  btnPoints.addEventListener('click', dataPoints)
  
  let btnProduct  = document.createElement('input') // id input-search
  btnProduct.setAttribute('type','button') 
  btnProduct.setAttribute('id','product')
  btnProduct.setAttribute('value','product')
  container_button.appendChild(btnProduct)
  btnProduct.addEventListener('click', dataProduct)

  let container_search = document.createElement('div')
  container_search.classList.add('input')
  container_search.setAttribute('id','box_search')
  main.appendChild(container_search)
  let search = document.createElement('input') // id input-search
  search.classList.add('input')
  search.setAttribute('id','search') 
  search.setAttribute('type','text') 
  search.setAttribute('name','search')
  search.setAttribute('placeholder','Effectuez une recherche')
  container_search.appendChild(search)
   search.addEventListener('input', filter)
   let btnSave = document.createElement('input') // id input-search
  btnSave.setAttribute('type','button') 
  btnSave.setAttribute('value','Save')
  btnSave.setAttribute('id','btn-save')
  container_search.appendChild(btnSave)
  btnSave.addEventListener('click', save)

  
  
  let containerTab = document.createElement('div')
  containerTab.classList.add('table_content')
  main.appendChild(containerTab)
  let boxTab = document.createElement('div')
  containerTab.appendChild(boxTab)
  boxTab.classList.add('container')
  boxTab.setAttribute('id','area_tab')
  

  let btnAddRow = document.createElement('input') // id input-search
  btnAddRow.setAttribute('type','button') 
  btnAddRow.setAttribute('value','Ajouter')
  btnAddRow.classList.add('btn_add_rows')
  containerTab.appendChild(btnAddRow)
  btnAddRow.addEventListener('click', ajouter)
}
dataPerson()